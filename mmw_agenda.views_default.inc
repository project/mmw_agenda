<?php

/**
 * Helper to implementation of hook_views_default_views().
 */
function mmw_agenda_views_default_views() {

  $views = array();
  // Conimbo voting.
  $view = new view;
  $view->name = 'Agenda';
  $view->description = 'List of agenda items ( simple agenda )';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('sorts', array(
    'field_agenda_date_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_agenda_date_value',
      'table' => 'node_data_field_agenda_date',
      'field' => 'field_agenda_date_value',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'agenda' => 'agenda',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'language' => array(
      'operator' => 'in',
      'value' => array(
        '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'language',
      'table' => 'node',
      'field' => 'language',
      'relationship' => 'none',
    ),
    'field_agenda_date_value' => array(
      'operator' => '>=',
      'value' => array(
        'value' => NULL,
        'min' => NULL,
        'max' => NULL,
        'default_date' => 'now',
        'default_to_date' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'date_fields' => array(
        'node_data_field_agenda_date.field_agenda_date_value' => 'node_data_field_agenda_date.field_agenda_date_value',
      ),
      'date_method' => 'AND',
      'granularity' => 'day',
      'form_type' => 'date_select',
      'default_date' => 'now',
      'default_to_date' => '',
      'year_range' => '-0:+3',
      'id' => 'field_agenda_date_value',
      'table' => 'node_data_field_agenda_date',
      'field' => 'field_agenda_date_value',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('row_plugin', 'node');
  $handler->override_option('row_options', array(
    'relationship' => 'none',
    'build_mode' => 'full',
    'links' => 1,
    'comments' => 0,
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('items_per_page', 3);
  $handler->override_option('row_plugin', 'ds');
  $handler->override_option('row_options', array(
    'default_fieldset' => array(
      'build_mode' => 'block',
    ),
    'changing_fieldset' => array(
      'changing' => 0,
      'allpages' => 0,
      'item_0' => 'teaser',
      'item_1' => 'teaser',
      'item_2' => 'teaser',
    ),
    'grouping_fieldset' => array(
      'grouping' => 0,
      'group_field' => 'node_data_field_agenda_date_field_agenda_date_value',
    ),
    'advanced_fieldset' => array(
      'advanced' => 0,
    ),
    'build_mode' => 'block',
    'changing' => 0,
    'grouping' => 0,
    'advanced' => 0,
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('title', 'Agenda');
  $handler->override_option('use_pager', '1');
  $handler->override_option('row_options', array(
    'relationship' => 'none',
    'build_mode' => 'teaser',
    'links' => 1,
    'comments' => 0,
  ));
  $handler->override_option('path', 'agenda');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  return $views;
}
