<?php
/**
 * @file
 * Display suite default settings.
 */

function _mmw_agenda_ds_default_settings() {
  $data = array(
    'nd' => array(
      'agenda' => array(
        'full' => array(
          'fields' => array(
            'addthis' => array(
              'region' => 'disabled',
              'weight' => '-90',
              'format' => 'conimbo_addthis_create_button',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'url_link' => array(
              'region' => 'disabled',
              'weight' => '-94',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'gallery_thumbnails' => array(
              'region' => 'disabled',
              'weight' => '-93',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'gallery_slideshow' => array(
              'region' => 'disabled',
              'weight' => '-92',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'related_blog_posts' => array(
              'region' => 'disabled',
              'weight' => '-91',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'field_agenda_date' => array(
              'region' => 'header',
              'weight' => '-100',
              'format' => 'long',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'title' => array(
              'region' => 'disabled',
              'weight' => '-100',
              'format' => 'nd_title_h2_block_link',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'author' => array(
              'region' => 'disabled',
              'weight' => '-97',
              'format' => 'ds_author_nolink',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'links' => array(
              'region' => 'disabled',
              'weight' => '-98',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'read_more' => array(
              'region' => 'disabled',
              'weight' => '-96',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'post_date' => array(
              'region' => 'disabled',
              'weight' => '-95',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'body' => array(
              'region' => 'middle',
              'weight' => '-99',
              'format' => 'nd_bodyfield',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
          ),
          'emptyregionrender' => array(
            'render' => array(
              'header' => 0,
              'left' => 0,
              'middle' => 0,
              'right' => 0,
              'footer' => 0,
            ),
          ),
          'status' => 2,
          'region_styles' => array(
            'header' => '',
            'left' => '',
            'middle' => '',
            'right' => '',
            'footer' => '',
          ),
        ),
        'block' => array(
          'fields' => array(
            'addthis' => array(
              'region' => 'disabled',
              'weight' => '-89',
              'format' => 'conimbo_addthis_create_button',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'url_link' => array(
              'region' => 'disabled',
              'weight' => '-93',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'gallery_thumbnails' => array(
              'region' => 'disabled',
              'weight' => '-92',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'gallery_slideshow' => array(
              'region' => 'disabled',
              'weight' => '-91',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'related_blog_posts' => array(
              'region' => 'disabled',
              'weight' => '-90',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'field_agenda_date' => array(
              'region' => 'middle',
              'weight' => '-99',
              'format' => 'short',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'title' => array(
              'region' => 'header',
              'weight' => '-100',
              'format' => 'nd_title_h2_block_link',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'author' => array(
              'region' => 'disabled',
              'weight' => '-96',
              'format' => 'ds_author_nolink',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'links' => array(
              'region' => 'disabled',
              'weight' => '-97',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'read_more' => array(
              'region' => 'disabled',
              'weight' => '-98',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'post_date' => array(
              'region' => 'disabled',
              'weight' => '-95',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'body' => array(
              'region' => 'disabled',
              'weight' => '-94',
              'format' => 'nd_bodyfield',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
          ),
          'emptyregionrender' => array(
            'render' => array(
              'header' => 0,
              'left' => 0,
              'middle' => 0,
              'right' => 0,
              'footer' => 0,
            ),
          ),
          'status' => 2,
          'region_styles' => array(
            'header' => '',
            'left' => '',
            'middle' => '',
            'right' => '',
            'footer' => '',
          ),
        ),
        'teaser' => array(
          'fields' => array(
            'addthis' => array(
              'region' => 'disabled',
              'weight' => '-89',
              'format' => 'conimbo_addthis_create_button',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'url_link' => array(
              'region' => 'disabled',
              'weight' => '-93',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'gallery_thumbnails' => array(
              'region' => 'disabled',
              'weight' => '-92',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'gallery_slideshow' => array(
              'region' => 'disabled',
              'weight' => '-91',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'related_blog_posts' => array(
              'region' => 'disabled',
              'weight' => '-90',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'field_agenda_date' => array(
              'region' => 'middle',
              'weight' => '-99',
              'format' => 'medium',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'title' => array(
              'region' => 'header',
              'weight' => '-100',
              'format' => 'nd_title_h2_block_link',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'author' => array(
              'region' => 'disabled',
              'weight' => '-95',
              'format' => 'ds_author_nolink',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'links' => array(
              'region' => 'disabled',
              'weight' => '-96',
              'format' => '',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'read_more' => array(
              'region' => 'footer',
              'weight' => '-97',
              'format' => 'ds_eval_code',
              'css-class' => 'button',
              'labelformat' => 'hidden',
            ),
            'post_date' => array(
              'region' => 'disabled',
              'weight' => '-94',
              'format' => 'ds_eval_code',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
            'body' => array(
              'region' => 'middle',
              'weight' => '-98',
              'format' => 'conimbo_teaser',
              'css-class' => '',
              'labelformat' => 'hidden',
            ),
          ),
          'emptyregionrender' => array(
            'render' => array(
              'header' => 0,
              'left' => 0,
              'middle' => 0,
              'right' => 0,
              'footer' => 0,
            ),
          ),
          'status' => 2,
          'region_styles' => array(
            'header' => '',
            'left' => '',
            'middle' => '',
            'right' => '',
            'footer' => '',
          ),
        ),
      ),
    ),
  );
  return $data;
}

function _mmw_agenda_ds_fields() {
  $data = array(
    'nd' => array(
      'post_date' => array(
        'title' => 'Post date',
        'exclude' => array(
          'error' => 0,
          'news' => 0,
          'page' => 0,
          'faq' => 0,
          'views_text' => 0,
          'gallery' => 0,
          'blog' => 0,
        ),
        'type' => 5,
        'status' => 2,
        'properties' => array(
          'formatters' => array(
            'ds_eval_code' => 'Default',
          ),
          'code' => '<?php
$langcode = isset($object->language) ? $object->language : \'en\';
echo format_date($object->created, "custom", "d M Y", NULL, $langcode);
?>',
          'token' => 0,
        ),
      ),
    ),
  );
  return $data;
}
